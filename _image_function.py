import cv2
import numpy as np
import _glove_function
import model
import common
import cache


def slice_image(img):
    img_fingers = img.copy()
    img_rest = img.copy()

    contour = _glove_function.find_glove()
    cv2.drawContours(img_fingers, [contour], 0, (0, 0, 255), 1)

    approx = cv2.approxPolyDP(contour, 0.02 * cv2.arcLength(contour, True), True)
    cv2.drawContours(img_fingers, [approx], 0, (255, 0, 0), 1)
    cv2.drawContours(cache.current_session.img_copy, [approx], 0, (255, 0, 0), 1)

    hull = cv2.convexHull(approx, returnPoints=True)
    cv2.drawContours(img_fingers, [hull], 0, (0, 0, 255), 2)
    cv2.drawContours(cache.current_session.img_copy, [hull], 0, (0, 0, 255), 2)

    # hull_def = cv2.convexHull(approx, returnPoints=False)
    # defect_count = cv2.convexityDefects(contour, hull_def)

    if len(hull) == 7 and len(approx) == 11:
        for i in range(11):
            glove = model.GloveModel(approx, i)

            if glove.is_valid_glove() and [glove.i0x, glove.i0y] in hull:
                rect = cv2.minAreaRect(contour)
                box = cv2.boxPoints(rect)
                box = np.int0(box)

                cv2.drawContours(img_fingers, [box], 0, (0, 255, 0), 2)
                cv2.drawContours(cache.current_session.img_copy, [box], 0, (0, 255, 0), 2)

                # Length
                cv2.line(img_fingers, (glove.mid_fin_x, glove.mid_fin_y), (glove.mid_base_x, glove.mid_base_y), (0, 255, 0), 1)
                cv2.line(cache.current_session.img_copy, (glove.mid_fin_x, glove.mid_fin_y),
                         (glove.mid_base_x, glove.mid_base_y), (0, 255, 0), 1)

                # mid_length = common.get_distance(glove.mid_fin_x, glove.mid_fin_y, glove.mid_base_x, glove.mid_base_y)
                # mid_x = (glove.mid_fin_x + glove.mid_base_x) / 2
                # mid_y = (glove.mid_fin_y + glove.mid_base_y) / 2

                thumb = _glove_function.get_finger_1(glove, contour, img_fingers)
                thumb_rect = cv2.minAreaRect(thumb)
                thumb_box = cv2.boxPoints(thumb_rect)
                thumb_box = np.int0(thumb_box)
                cv2.drawContours(img_fingers, [thumb_box], 0, (0, 255, 255), 1)

                index_finger = _glove_function.get_finger_2(glove, contour)
                index_rect = cv2.minAreaRect(index_finger)
                index_box = cv2.boxPoints(index_rect)
                index_box = np.int0(index_box)
                cv2.drawContours(img_fingers, [index_box], 0, (0, 255, 255), 1)

                middle_finger = _glove_function.get_finger_3(glove, contour)
                middle_rect = cv2.minAreaRect(middle_finger)
                middle_box = cv2.boxPoints(middle_rect)
                middle_box = np.int0(middle_box)
                cv2.drawContours(img_fingers, [middle_box], 0, (0, 255, 255), 1)

                ring_finger = _glove_function.get_finger_4(glove, contour)
                ring_rect = cv2.minAreaRect(ring_finger)
                ring_box = cv2.boxPoints(ring_rect)
                ring_box = np.int0(ring_box)
                cv2.drawContours(img_fingers, [ring_box], 0, (0, 255, 255), 1)

                pinkey_finger = _glove_function.get_finger_5(glove, contour, img_fingers)
                pinkey_rect = cv2.minAreaRect(pinkey_finger)
                pinkey_box = cv2.boxPoints(pinkey_rect)
                pinkey_box = np.int0(pinkey_box)
                cv2.drawContours(img_fingers, [pinkey_box], 0, (0, 255, 255), 1)

                detailed_approx = cv2.approxPolyDP(contour, 0.007 * cv2.arcLength(contour, True), True)
                cv2.drawContours(img_rest, [detailed_approx], 0, (255, 255, 0), 1)

                palm = _glove_function.get_palm(glove, detailed_approx, img_rest)
                palm_rect = cv2.minAreaRect(palm)
                palm_box = cv2.boxPoints(palm_rect)
                palm_box = np.int0(palm_box)
                cv2.drawContours(img_rest, [palm_box], 0, (255, 0, 255), 2)

                wrist = _glove_function.get_wrist(glove, detailed_approx, img_rest)
                wrist_rest = cv2.minAreaRect(wrist)
                wrist_box = cv2.boxPoints(wrist_rest)
                wrist_box = np.int0(wrist_box)
                cv2.drawContours(img_rest, [wrist_box], 0, (0, 255, 255), 1)

                break

    common.write_image(img_fingers, cache.current_session.output_path, 'slice_fingers')
    common.write_image(img_rest, cache.current_session.output_path, 'slice_rest')