import cv2
import time


def pi_video():
    from picamera.array import PiRGBArray
    from picamera import PiCamera
    import cv2
    import time

    camera = PiCamera()
    camera.resolution = (640, 480)
    camera.framerate = 32
    rawCapture = PiRGBArray(camera, size=(640, 480))

    time.sleep(0.1)

    for frame in camera.capture_continuous(rawCapture, format="rbg", use_video_port=True):
        image = frame.array

        cv2.imshow("Frame", image)
        key = cv2.waitKey(1) & 0xFF

        rawCapture.truncate(0)

        if key == ord("q"):
            break


def pi_image():
    from picamera.array import PiRGBArray
    from picamera import PiCamera

    camera = PiCamera()
    rawCapture = PiRGBArray(camera)

    time.sleep(0.1)

    camera.capture(rawCapture, format="bgr")
    image = rawCapture.array

    cv2.imshow("Image", image)
    cv2.waitKey(0)


def pi_camera_input():
    from picamera.array import PiRGBArray
    from picamera import PiCamera

    camera = PiCamera()
    camera.resolution = (640, 480)
    camera.framerate = 60
    rawCapture = PiRGBArray(camera, size=(640, 480))

    time.sleep(0.1)

    image = None
    for frame in camera.capture_continuous(rawCapture, format="rgb", use_video_port=True):
        image = frame.array

        cv2.imshow("Feed", image)
        rawCapture.truncate(0)

        # Finding the glove ############################################################################################
        print 'Finding the glove....'
        ################################################################################################################
        if cv2.waitKey(1) % 256 == 27:
            break