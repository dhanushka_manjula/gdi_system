import math
import cv2


def div(numerator, denominator):
    if denominator == 0.0:
        if numerator > 0:
            return float('Inf')
        else:
            return float('-Inf')
    else:
        return numerator / denominator


def get_gradient(x1, y1, x2, y2):
    dx = float(x2 - x1)
    dy = y2 - y1
    return div(dy, dx)


def get_distance(x1, y1, x2, y2):
    return math.sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2))


def get_angle_between(m1, m2):
    denominator = float(1.0 + m1 * m2)
    if denominator == 0.0:
        return 90
    return math.degrees(abs(math.atan(abs(m1 - m2) / denominator)))


def write_image(image, path, suffix):
    if path is not None:
        if path.endswith(".jpg"):
            new_path = path[:len(path) - 4] + '_' + str(suffix) + path[len(path) - 4:]
        elif path.endswith(".jpeg"):
            new_path = path[:len(path) - 4] + '_' + str(suffix) + path[len(path) - 5:]
        cv2.imwrite(new_path, image)


def is_cont_in(contour, contour_set):
    for c in contour_set:
        if contour[0][0] == c[0][0] and contour[0][1] == c[0][1]:
            return True
    return False


def cont_equals(contour1, contour2):
    return contour1[0][0] == contour2[0][0] and contour1[0][1] == contour2[0][1]