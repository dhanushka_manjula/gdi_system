import cv2
import numpy as np
import common
import _glove_function
import model
import global_values
import time
import cache


def perfect_shape(img):
    perfect_glove_found = False

    contour = _glove_function.find_glove()
    cv2.drawContours(img, [contour], 0, (0, 0, 255), 1)

    approx = cv2.approxPolyDP(contour, 0.02 * cv2.arcLength(contour, True), True)
    cv2.drawContours(img, [approx], 0, (255, 0, 0), 1)
    cv2.drawContours(cache.current_session.img_copy, [approx], 0, (255, 0, 0), 1)

    hull = cv2.convexHull(approx, returnPoints=True)
    cv2.drawContours(img, [hull], 0, (0, 0, 255), 2)
    cv2.drawContours(cache.current_session.img_copy, [hull], 0, (0, 0, 255), 2)

    # hull_def = cv2.convexHull(approx, returnPoints=False)
    # defect_count = cv2.convexityDefects(contour, hull_def)

    if len(hull) == 7 and len(approx) == 11:
        for i in range(11):
            # print i
            glove = model.GloveModel(approx, i)

            if glove.is_valid_glove() and [glove.i0x, glove.i0y] in hull:
                cache.current_session.glove = glove
                # print (i)
                # cv2.circle(img, (approx[i][0][0], approx[i][0][1]), 8, (255, 0, 255), cv2.FILLED)
                # cv2.circle(img, (approx[(i + 7) % 11][0][0], approx[(i + 7) % 11][0][1]), 8, (255, 0, 255),
                #            cv2.FILLED)
                # cv2.circle(img, (approx[(i + 3) % 11][0][0], approx[(i + 3) % 11][0][1]), 8, (255, 0, 255),
                #            cv2.FILLED)
                # cv2.circle(img, (approx[(i + 5) % 11][0][0], approx[(i + 5) % 11][0][1]), 8, (255, 0, 255),
                #            cv2.FILLED)
                # cv2.circle(img, (approx[(i + 9) % 11][0][0], approx[(i + 9) % 11][0][1]), 8, (255, 0, 255),
                #            cv2.FILLED)
                # cv2.circle(img, (approx[(i + 11) % 11][0][0], approx[(i + 11) % 11][0][1]), 8, (255, 0, 255),
                #            cv2.FILLED)

                # cv2.putText(img, 'T', (approx[(i + 11) % 11][0][0], approx[(i + 11) % 11][0][1]), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1.5,
                #             (0, 0, 255))
                # cv2.putText(img, 'I', (approx[(i + 9) % 11][0][0], approx[(i + 9) % 11][0][1]), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1.5,
                #             (0, 0, 255))
                # cv2.putText(img, 'M', (approx[(i + 7) % 11][0][0], approx[(i + 7) % 11][0][1]), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1.5,
                #             (0, 0, 255))
                # cv2.putText(img, 'R', (approx[(i + 5) % 11][0][0], approx[(i + 5) % 11][0][1]), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1.5,
                #             (0, 0, 255))
                # cv2.putText(img, 'P', (approx[(i + 3) % 11][0][0], approx[(i + 3) % 11][0][1]), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1.5,
                #             (0, 0, 255))


                perfect_glove_found = True

                cv2.putText(img, 'X', (glove.i0x + 10, glove.i0y + 10), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1.5,
                            (0, 0, 255))
                cv2.putText(img, 'Y', (glove.i1x + 10, glove.i1y + 10), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1.5,
                            (0, 0, 255))

                cv2.putText(cache.current_session.img_copy, 'X', (glove.i0x + 10, glove.i0y + 10),
                            cv2.FONT_HERSHEY_COMPLEX_SMALL, 1.5,
                            (0, 0, 255))
                cv2.putText(cache.current_session.img_copy, 'Y', (glove.i1x + 10, glove.i1y + 10),
                            cv2.FONT_HERSHEY_COMPLEX_SMALL, 1.5,
                            (0, 0, 255))

                rect = cv2.minAreaRect(contour)
                box = cv2.boxPoints(rect)
                box = np.int0(box)

                cv2.drawContours(img, [box], 0, (0, 255, 0), 2)
                cv2.drawContours(cache.current_session.img_copy, [box], 0, (0, 255, 0), 2)

                # Length
                cv2.line(img, (glove.mid_fin_x, glove.mid_fin_y), (glove.mid_base_x, glove.mid_base_y), (0, 255, 0), 1)
                cv2.line(cache.current_session.img_copy, (glove.mid_fin_x, glove.mid_fin_y),
                         (glove.mid_base_x, glove.mid_base_y), (0, 255, 0), 1)

                mid_length = common.get_distance(glove.mid_fin_x, glove.mid_fin_y, glove.mid_base_x, glove.mid_base_y)
                mid_x = (glove.mid_fin_x + glove.mid_base_x) / 2
                mid_y = (glove.mid_fin_y + glove.mid_base_y) / 2
                cv2.putText(img, str(mid_length), (mid_x, mid_y), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8,
                            (0, 255, 0))
                cv2.putText(cache.current_session.img_copy, str(mid_length), (mid_x, mid_y),
                            cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8,
                            (0, 255, 0))

                cv2.putText(img, 'Perfect shape', (25, 50), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8,
                            (255, 0, 0))
                cv2.putText(cache.current_session.img_copy, 'Perfect shape', (25, 50), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8,
                            (255, 0, 0))
                # cv2.putText(img, str(len(defect_count)), (25, 50), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8,
                #             (255, 0, 0))
                break

        if perfect_glove_found:
            for i in range(11):
                g = model.GloveModel(approx, i)
                cv2.putText(img, str(format(g.angle1, '.2f')) + ' deg.', (g.i1x + 10, g.i1y + 10),
                            cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8, (0, 0, 255))
                # cv2.putText(cache.current_session.img_copy, str(format(g.angle1, '.2f')) + ' deg.',
                #             (g.i1x + 10, g.i1y + 10),
                #             cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8, (0, 0, 255))
            common.write_image(img, cache.current_session.output_path, 'is_perfect')
            common.write_image(img, './output/good/' + str(time.time()) + '.jpg', 'is_perfect')

    if not perfect_glove_found:
        cv2.putText(img, 'Incorrect shape', (25, 50), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8,
                    (0, 0, 255))
        cv2.putText(cache.current_session.img_copy, 'Incorrect shape', (25, 50), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8,
                    (0, 0, 255))
        common.write_image(img, cache.current_session.output_path, 'is_perfect')
        common.write_image(img, './output/bad/' + str(time.time()) + '.jpg', 'is_perfect')

    cv2.imshow('Processed video', img)

    return perfect_glove_found


def dropped_stitch_contours(img):
    contours, hierarchy = cache.current_session.contours_dilated_list, cache.current_session.hierarchy_dilated_list
    for c in contours:
        cv2.drawContours(img, [c], 0, (0, 255, 0), 1)

    selected_contours = []
    selected_hierarchies = []
    for i in range(len(contours)):
        c = contours[i]
        h = hierarchy[0][i]
        approx = cv2.approxPolyDP(c, 0.02 * cv2.arcLength(c, True), True)
        hull = cv2.convexHull(approx, returnPoints=True)

        # if len(hull) > 4:
        if len(hull) > 5:
            selected_contours.append(c)
            selected_hierarchies.append(h)

    areas = [cv2.contourArea(cnt) for cnt in selected_contours]
    max_index = np.argmax(areas)
    largest_contour = selected_contours[max_index]
    largest_hierarchy = selected_hierarchies[max_index]

    dropped_stitch_absent = True
    if largest_hierarchy[2] != -1:
        for i in range(len(contours)):
            c = contours[i]
            h = hierarchy[0][i]
            if h[3] != -1:
                cv2.drawContours(img, [c], 0, (0, 0, 255), 1)
                cv2.drawContours(cache.current_session.img_copy, [c], 0, (0, 0, 255), 2)
        dropped_stitch_absent = False
        cv2.putText(img, 'Dropped stitch', (25, 50), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8,
                    (0, 0, 255))
        cv2.putText(cache.current_session.img_copy, 'Dropped stitch', (25, 75), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8,
                    (0, 0, 255))
        # common.write_image(img, global_values.temp_output_path, 'dropped_stitch')
        # common.write_image(img, './output/bad/' + str(time.time()) + '.jpg', 'dropped_stitch')
    else:
        cv2.drawContours(img, [largest_contour], 0, (255, 0, 0), 2)
        cv2.putText(img, 'No dropped stitch', (25, 50), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8,
                    (255, 0, 0))
        cv2.putText(cache.current_session.img_copy, 'No dropped stitch', (25, 75), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8,
                    (255, 0, 0))
        # common.write_image(img, global_values.temp_output_path, 'dropped_stitch')
        # common.write_image(img, './output/good/' + str(time.time()) + '.jpg', 'dropped_stitch')

    return dropped_stitch_absent


# def dropped_stitch(img):
#     gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
#
#     returned, threshold_img = cv2.threshold(gray_img, 140, 255, cv2.THRESH_BINARY)
#     common.write_image(threshold_img, global_values.temp_output_path, 'dropped_stitch_thresh')
#
#     blurred_img = cv2.GaussianBlur(gray_img, (3, 3), 0)
#     # returned, threshold_img = cv2.threshold(gray_img, 127, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
#     # common.write_image(threshold_img, global_values.temp_output_path, 'dropped_stitch_otsu')
#
#     cont_img = img.copy()
#     kernel = np.ones((3, 3), np.uint8)
#     dilated_img = cv2.dilate(threshold_img, kernel, iterations=1)
#     _, contours, hierarchy = cv2.findContours(cont_img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
#     for c in contours:
#         cv2.drawContours(cont_img, [c], 0, (0, 0, 255), 1)
#     common.write_image(cont_img, global_values.temp_output_path, 'dropped_stitch_cont')


def color_yarn_damage(img):
    return True


# Obsolete
def needle_run_old(img):
    _, contours, hierarchy = cache.current_session.contours_dilated_list, cache.current_session.hierarchy_dilated_list

    needle_run_found = False
    if len(contours) >= 2:
        areas = [cv2.contourArea(cnt) for cnt in contours]
        max_index = np.argmax(areas)
        selected_contours = contours[:max_index] + contours[max_index + 1:]

        contours = selected_contours[::]
        areas = [cv2.contourArea(cnt) for cnt in contours]
        max_index = np.argmax(areas)
        selected_contours = contours[:max_index] + contours[max_index + 1:]

        for cnt in selected_contours:
            approx = cv2.approxPolyDP(cnt, 0.2 * cv2.arcLength(cnt, True), True)
            area = cv2.contourArea(cnt)

            if len(approx) == 2:
                dist = common.get_distance(approx[0][0][0], approx[0][0][1], approx[1][0][0], approx[1][0][1])
                if dist > 100:
                    needle_run_found = True
                    cv2.drawContours(img, [approx], 0, (0, 0, 255), 2)
                    cv2.drawContours(cache.current_session.img_copy, [approx], 0, (0, 0, 255), 2)

        if needle_run_found:
            cv2.putText(img, 'Needle run', (25, 50), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8,
                        (0, 0, 255))
            cv2.putText(cache.current_session.img_copy, 'Needle run', (25, 100), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8,
                        (0, 0, 255))
            common.write_image(img, './output/bad/' + str(time.time()) + '.jpg', 'needle_run')
        else:
            cv2.putText(img, 'No needle run', (25, 50), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8,
                        (255, 0, 0))
            cv2.putText(cache.current_session.img_copy, 'No needle run', (25, 100), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8,
                        (255, 0, 0))
            common.write_image(img, cache.current_session.output_path, 'needle_run')
            common.write_image(img, './output/good/' + str(time.time()) + '.jpg', 'needle_run')

    return not needle_run_found


def needle_run(img):
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    returned, threshold_img = cv2.threshold(gray_img, 140, 255, cv2.THRESH_BINARY)
    common.write_image(threshold_img, cache.current_session.output_path, 'needle_thresh')

    returned, threshold_img = cv2.threshold(gray_img, 127, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

    kernel = np.ones((3, 3), np.uint8)
    dilated_img = cv2.dilate(threshold_img, kernel, iterations=1)
    eroded_img = cv2.erode(dilated_img, kernel, iterations=1)
    common.write_image(dilated_img, cache.current_session.output_path, 'needle_dil')
    common.write_image(eroded_img, cache.current_session.output_path, 'needle_ero')

    _, contours, hierarchy = cv2.findContours(dilated_img.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    needle_run_found = False
    if len(contours) >= 2:
        areas = [cv2.contourArea(cnt) for cnt in contours]
        max_index = np.argmax(areas)
        selected_contours = contours[:max_index] + contours[max_index + 1:]

        contours = selected_contours[::]
        areas = [cv2.contourArea(cnt) for cnt in contours]
        max_index = np.argmax(areas)
        selected_contours = contours[:max_index] + contours[max_index + 1:]

        for cnt in selected_contours:
            approx = cv2.approxPolyDP(cnt, 0.2 * cv2.arcLength(cnt, True), True)
            area = cv2.contourArea(cnt)

            if len(approx) == 2:
                print ("#############")
                dist = common.get_distance(approx[0][0][0], approx[0][0][1], approx[1][0][0], approx[1][0][1])
                print ("DIST " + str(dist))
                if dist > 100:
                    needle_run_found = True

                    cv2.drawContours(img, [approx], 0, (0, 0, 255), 2)
                    cv2.drawContours(cache.current_session.img_copy, [approx], 0, (0, 0, 255), 2)

        if needle_run_found:
            cv2.putText(img, 'Needle run', (25, 50), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8,
                        (0, 0, 255))
            cv2.putText(cache.current_session.img_copy, 'Needle run', (25, 100), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8,
                        (0, 0, 255))
            common.write_image(img, cache.current_session.output_path, 'needle_run')
            common.write_image(img, './output/bad/' + str(time.time()) + '.jpg', 'needle_run')
            print('Needle run')
        else:
            cv2.putText(img, 'No needle run', (25, 50), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8,
                        (255, 0, 0))
            cv2.putText(cache.current_session.img_copy, 'No needle run', (25, 100), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8,
                        (255, 0, 0))
            common.write_image(img, cache.current_session.output_path, 'needle_run')
            common.write_image(img, './output/good/' + str(time.time()) + '.jpg', 'needle_run')
            print ('No needle run')

    return not needle_run_found

def needle_run_new(img):
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    returned, threshold_img = cv2.threshold(gray_img, 160, 255, cv2.THRESH_BINARY)
    common.write_image(threshold_img, cache.current_session.output_path, 'needle_thresh')
    # cv2.imshow("FFF", threshold_img)
    # returned, threshold_img = cv2.threshold(gray_img, 127, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

    # kernel = np.ones((3, 3), np.uint8)
    # dilated_img = cv2.dilate(threshold_img, kernel, iterations=1)
    # eroded_img = cv2.erode(dilated_img, kernel, iterations=1)
    # common.write_image(dilated_img, cache.current_session.output_path, 'needle_dil')
    # common.write_image(eroded_img, cache.current_session.output_path, 'needle_ero')

    _, contours, hierarchy = cv2.findContours(threshold_img.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

    needle_run_found = False
    if len(contours) >= 2:
        areas = []
        selec_contours = []
        for cnt in contours:
            area = cv2.contourArea(cnt)
            if  area> 5 and area < 80000:
                selec_contours.append(cnt)

        # cv2.Houg
        # max_index = np.argmax(areas)
        # selected_contours = contours[:max_index] + contours[max_index + 1:]

        # contours = selected_contours[::]
        # areas = [cv2.contourArea(cnt) for cnt in selec_contours]
        # max_index = np.argmax(areas)
        # print("AREA " + str(areas[max_index]))
        # selected_contours = contours[:max_index] + contours[max_index + 1:]

        # glove = cache.current_session.glove
        # mid_gradiant = common.get_gradient(glove.mid_fin_x, glove.mid_fin_y, glove.mid_base_x, glove.mid_base_y)
        # print ("GRAD " + str(mid_gradiant))

        needle_run_contours = []
        total_length = 0
        for cnt in selec_contours:
            # M = cv2.moments(cnt)
            # cx = int(M['m10'] / M['m00'])
            # cy = int(M['m01'] / M['m00'])
            # centroids.append([cx, cy])

            rect = cv2.minAreaRect(cnt)
            box = cv2.boxPoints(rect)
            # print ("BOX " + str(rect[1][0]))
            # box = np.int0(box)
            # cv2.drawContours(img, [box], 0, (0, 0, 255), 2)
            if rect[1][0] < 4:
                needle_run_contours.append(cnt)
                total_length += rect[1][1]
                img = cv2.line(img, (box[0][0], box[0][1]), (box[1][0], box[1][1]), (0, 0, 255), 2)

        # print("LEGTH " + str(total_length))
        needle_run_found = False
        if total_length > 70:
            needle_run_found = True

            # rows, cols = img.shape[:2]
            # [vx, vy, x, y] = cv2.fitLine(cnt, cv2.DIST_L2, 0, 0.01, 0.01)
            # lefty = int(y)
            # righty = int((x * vy / vx) + y)
            # img = cv2.line(img, (x, righty), (, lefty), (0, 255, 0), 2)
            # gradiant = common.get_gradient(box[0][0], box[0][1], box[1][0], box[1][1])


            # if gradiant < mid_gradiant - 3

            # print ("RECT " + str(rect[1][0]))
            print("____________________")

            # cv2.waitKey(0)
            # approx = cv2.approxPolyDP(cnt, 0.2 * cv2.arcLength(cnt, True), True)
            # area = cv2.contourArea(cnt)

        # print (centroids)

        # gradiants = []
        # for i in range(len(centroids)-1):
        #
        #     print ("GRADIANT " + str(common.get_gradient(centroids[i][0],centroids[i][1],centroids[i+1][0],centroids[i+1][1])))


            # if len(approx) == 2:
            #     print ("#############")
            #     dist = common.get_distance(approx[0][0][0], approx[0][0][1], approx[1][0][0], approx[1][0][1])
            #     print ("DIST " + str(dist))
            #     if dist > 100:
            #         needle_run_found = True
            #
            #         cv2.drawContours(img, [approx], 0, (0, 0, 255), 2)
            #         cv2.drawContours(cache.current_session.img_copy, [approx], 0, (0, 0, 255), 2)

        if needle_run_found:
            cv2.putText(img, 'Needle run', (25, 50), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8,
                        (0, 0, 255))
            cv2.putText(cache.current_session.img_copy, 'Needle run', (25, 100), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8,
                        (0, 0, 255))
            common.write_image(img, cache.current_session.output_path, 'needle_run')
            common.write_image(img, './output/bad/' + str(time.time()) + '.jpg', 'needle_run')
            print('Needle run')
        else:
            cv2.putText(img, 'No needle run', (25, 50), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8,
                        (255, 0, 0))
            cv2.putText(cache.current_session.img_copy, 'No needle run', (25, 100), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8,
                        (255, 0, 0))
            common.write_image(img, cache.current_session.output_path, 'needle_run')
            common.write_image(img, './output/good/' + str(time.time()) + '.jpg', 'needle_run')
            print ('No needle run')

    cv2.imshow("FFF", cache.current_session.img_copy)
    return not needle_run_found

def dropped_stitch_new(img):
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    returned, threshold_img = cv2.threshold(gray_img, 160, 255, cv2.THRESH_BINARY)
    common.write_image(threshold_img, cache.current_session.output_path, 'dropped_thresh')
    cv2.imshow("KKK", threshold_img)
    # returned, threshold_img = cv2.threshold(gray_img, 127, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

    # kernel = np.ones((3, 3), np.uint8)
    # dilated_img = cv2.dilate(threshold_img, kernel, iterations=1)
    # eroded_img = cv2.erode(dilated_img, kernel, iterations=1)
    # common.write_image(dilated_img, cache.current_session.output_path, 'needle_dil')
    # common.write_image(eroded_img, cache.current_session.output_path, 'needle_ero')

    _, contours, hierarchy = cv2.findContours(threshold_img.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

    dropped_stitch_found = False
    if len(contours) >= 2:
        areas = []
        selec_contours = []
        for cnt in contours:
            area = cv2.contourArea(cnt)
            print ("AREA " + str(area))
            if area > 5 and area < 40:
                selec_contours.append(cnt)

    dropped_stitch_contours = []
    # total_length = 0
    for cnt in selec_contours:
        # M = cv2.moments(cnt)
        # cx = int(M['m10'] / M['m00'])
        # cy = int(M['m01'] / M['m00'])
        # centroids.append([cx, cy])

        rect = cv2.minAreaRect(cnt)
        box = cv2.boxPoints(rect)
        # print ("BOX " + str(rect[1][0]))
        # box = np.int0(box)
        # cv2.drawContours(img, [box], 0, (0, 0, 255), 2)
        if rect[1][0] < 8 and rect[1][1] < 8:
            dropped_stitch_contours.append(cnt)
            # total_length += rect[1][1]
            dropped_stitch_found = True
            # img = cv2.line(img, (box[0][0], box[0][1]), (box[1][0], box[1][1]), (0, 0, 255), 2)
            box = np.int0(box)
            cv2.drawContours(cache.current_session.img_copy, [box], 0, (0, 0, 255), 2)


        print("RECT " + str(rect))
    # needle_run_found = False
    # if total_length > 70:
    #     needle_run_found = True

    # dropped_stitch_absent = True

    if dropped_stitch_found:

        cv2.putText(img, 'Dropped stitch', (25, 50), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8,
                    (0, 0, 255))
        cv2.putText(cache.current_session.img_copy, 'Dropped stitch', (25, 75), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8,
                    (0, 0, 255))
        # common.write_image(img, global_values.temp_output_path, 'dropped_stitch')
        # common.write_image(img, './output/bad/' + str(time.time()) + '.jpg', 'dropped_stitch')
    else:
        # cv2.drawContours(img, [largest_contour], 0, (255, 0, 0), 2)
        cv2.putText(img, 'No dropped stitch', (25, 50), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8,
                    (255, 0, 0))
        cv2.putText(cache.current_session.img_copy, 'No dropped stitch', (25, 75), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8,
                    (255, 0, 0))
        # common.write_image(img, global_values.temp_output_path, 'dropped_stitch')
        # common.write_image(img, './output/good/' + str(time.time()) + '.jpg', 'dropped_stitch')
    cv2.imshow("FFF", cache.current_session.img_copy)
    return dropped_stitch_found

def finger_tip_blow_out(img):
    return True
