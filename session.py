class Session:
    def __init__(self):
        self.output_path = None
        self.img_copy = None

        self.glove_model = None

        self.gray_img = None
        self.blurred_img_33 = None
        self.threshold_img_normal_l60 = None

        self.eroded_img_55 = None
        self.dilated_img_55 = None

        self.contours_dilated_list = None
        self.hierarchy_dilated_list = None
