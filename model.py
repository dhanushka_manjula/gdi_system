import common


class GloveModel:
    def __init__(self, contour, pos):
        self.contour = contour
        self.pos = pos
        # print (pos)

        self.i0x = contour[pos][0][0]
        self.i0y = contour[pos][0][1]
        self.i1x = contour[(pos + 1) % 11][0][0]
        self.i1y = contour[(pos + 1) % 11][0][1]
        self.i2x = contour[(pos + 2) % 11][0][0]
        self.i2y = contour[(pos + 2) % 11][0][1]
        self.i3x = contour[(pos + 3) % 11][0][0]
        self.i3y = contour[(pos + 3) % 11][0][1]

        # # 11
        # self.thumb_fin_x = contour[(pos + 11) % 11][0][0]
        # self.thumb_fin_y = contour[(pos + 11) % 11][0][1]
        #
        # # 10
        # self.thumb_index_x = contour[(pos + 10) % 11][0][0]
        # self.thumb_index_y = contour[(pos + 10) % 11][0][1]
        #
        # # 9
        # self.index_fin_x = contour[(pos + 9) % 11][0][0]
        # self.index_fin_y = contour[(pos + 9) % 11][0][1]
        #
        # # 8
        # self.index_mid_x = contour[(pos + 8) % 11][0][0]
        # self.index_mid_y = contour[(pos + 8) % 11][0][1]
        #
        # # 7
        self.mid_fin_x = contour[(pos + 7) % 11][0][0]
        self.mid_fin_y = contour[(pos + 7) % 11][0][1]
        #
        # # 6
        # self.mid_ring_x = contour[(pos + 6) % 11][0][0]
        # self.mid_ring_y = contour[(pos + 6) % 11][0][1]
        #
        # # 5
        # self.ring_fin_x = contour[(pos + 5) % 11][0][0]
        # self.ring_fin_y = contour[(pos + 5) % 11][0][1]
        #
        # # 4
        # self.ring_pinkie_x = contour[(pos + 4) % 11][0][0]
        # self.ring_pinkie_y = contour[(pos + 4) % 11][0][1]
        #
        # # 3
        # self.pinkie_fin_x = contour[(pos + 3) % 11][0][0]
        # self.pinkie_fin_y = contour[(pos + 3) % 11][0][1]

        self.mid_base_x = (self.i1x + self.i2x) / 2
        self.mid_base_y = (self.i1y + self.i2y) / 2

        self.m1 = common.get_gradient(self.i0x, self.i0y, self.i1x, self.i1y)
        self.m2 = common.get_gradient(self.i1x, self.i1y, self.i2x, self.i2y)
        self.m3 = common.get_gradient(self.i2x, self.i2y, self.i3x, self.i3y)

        # # 11-10
        # self.m11_10 = common.get_gradient(self.thumb_fin_x, self.thumb_fin_y, self.thumb_index_x, self.thumb_index_y)
        # # 10-9
        # self.m10_9 = common.get_gradient(self.thumb_index_x, self.thumb_index_y, self.index_fin_x, self.index_fin_y)
        # # 9-8
        # self.m9_8 = common.get_gradient(self.index_fin_x, self.index_fin_y, self.index_mid_x, self.index_mid_y)
        # # 8-7
        # self.m8_7 = common.get_gradient(self.index_mid_x, self.index_mid_y, self.mid_fin_x, self.mid_fin_y)
        # # 7-6
        # self.m7_6 = common.get_gradient(self.mid_fin_x, self.mid_fin_y, self.mid_ring_x, self.mid_ring_y)
        # # 6-5
        # self.m6_5 = common.get_gradient(self.mid_ring_x, self.mid_ring_y, self.ring_fin_x, self.ring_fin_y)
        # # 5-4
        # self.m5_4 = common.get_gradient(self.ring_fin_x, self.ring_fin_y, self.ring_pinkie_x, self.ring_pinkie_y)
        # # 4-3
        # self.m4_3 = common.get_gradient(self.ring_pinkie_x, self.ring_pinkie_y, self.pinkie_fin_x, self.pinkie_fin_y)

        self.angle1 = common.get_angle_between(self.m1, self.m2)
        self.angle2 = common.get_angle_between(self.m2, self.m3)

        # self.angle3 = common.get_angle_between(self.m9_8, self.m8_7)
        # print ("ANGLE " + str(self.angle3))

    def is_valid_glove(self):
        c1 = 70 <= self.angle1 <= 110
        c2 = 70 <= self.angle2 <= 110

        return c1 and c2
