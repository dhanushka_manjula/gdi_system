import cv2
import numpy as np
import common
import cache


def find_glove():
    contours = cache.current_session.contours_dilated_list

    selected_contours = []
    for c in contours:
        approx = cv2.approxPolyDP(c, 0.02 * cv2.arcLength(c, True), True)
        hull = cv2.convexHull(approx, returnPoints=True)

        # if len(hull) > 4:
        if len(hull) > 5:
            selected_contours.append(c)

    areas = [cv2.contourArea(cnt) for cnt in selected_contours]
    max_index = np.argmax(areas)
    largest_contour = selected_contours[max_index]

    return largest_contour


# Thumb
def get_finger_1(glove, detailed_cont, img):
    base_pts = []

    mid_0_1_x = (glove.contour[(glove.pos + 0) % 11][0][0] + glove.contour[(glove.pos + 1) % 11][0][0]) / 2
    mid_0_1_y = (glove.contour[(glove.pos + 0) % 11][0][1] + glove.contour[(glove.pos + 1) % 11][0][1]) / 2
    min_dist = common.get_distance(mid_0_1_x, mid_0_1_y, detailed_cont[0][0][0], detailed_cont[0][0][1])
    selected = detailed_cont[0]
    for k in detailed_cont:
        dist = common.get_distance(mid_0_1_x, mid_0_1_y, k[0][0], k[0][1])
        if dist < min_dist:
            min_dist = dist
            selected = k
    base_pts.append(selected)
    cv2.circle(img, (mid_0_1_x, mid_0_1_y), 5, (0, 0, 255), -1)
    cv2.circle(img, (selected[0][0], selected[0][1]), 5, (0, 0, 255), -1)

    cv2.circle(img, (glove.contour[(glove.pos + 0) % 11][0][0], glove.contour[(glove.pos + 0) % 11][0][1]), 5,
               (0, 255, 255), -1)
    cv2.circle(img, (glove.contour[(glove.pos + 10) % 11][0][0], glove.contour[(glove.pos + 10) % 11][0][1]), 5,
               (0, 255, 255), -1)

    base_pts.append(glove.contour[(glove.pos + 0) % 11])
    base_pts.append(glove.contour[(glove.pos + 10) % 11])

    all_pts = []
    left_point_met = False
    mid_point_met = False
    right_point_met = False
    first_met = None
    total_cont_count = len(detailed_cont)
    i = 0
    while i <= 2 * total_cont_count:
        current = detailed_cont[i % total_cont_count]
        if current[0][0] == glove.contour[(glove.pos + 10) % 11][0][0] and current[0][1] == \
                glove.contour[(glove.pos + 10) % 11][0][1]:
            left_point_met = True
            if first_met is None:
                first_met = 'left'
        elif current[0][0] == glove.contour[(glove.pos + 0) % 11][0][0] and current[0][1] == \
                glove.contour[(glove.pos + 0) % 11][0][1]:
            mid_point_met = True
            if first_met is None:
                first_met = 'mid'
        elif current[0][0] == selected[0][0] and current[0][1] == selected[0][1]:
            right_point_met = True
            if first_met is None:
                first_met = 'right'

        if left_point_met or right_point_met:
            all_pts.append(current)

        if left_point_met and not mid_point_met and right_point_met:
            all_pts = []
            if first_met == 'left':
                left_point_met = False
            elif first_met == 'right':
                right_point_met = False
        elif first_met == 'mid':
            all_pts = []
            first_met = None
            mid_point_met = False
        elif left_point_met and mid_point_met and right_point_met:
            break

        i += 1

    for p in all_pts:
        cv2.circle(img, (p[0][0], p[0][1]), 2, (255, 0, 255), -1)

    ctr = np.array(all_pts).reshape((-1, 1, 2)).astype(np.int32)
    return ctr


# Index finger
def get_finger_2(glove, detailed_cont):
    base_pts = []
    base_pts.append(glove.contour[(glove.pos + 8) % 11])
    base_pts.append(glove.contour[(glove.pos + 9) % 11])
    base_pts.append(glove.contour[(glove.pos + 10) % 11])

    in_range = False
    all_pts = []
    count = 0
    for i in range(len(detailed_cont)):
        if common.is_cont_in(detailed_cont[i], base_pts):
            in_range = True
            count += 1
        elif count == 3:
            break

        if in_range:
            all_pts.append(detailed_cont[i])

    ctr = np.array(all_pts).reshape((-1, 1, 2)).astype(np.int32)
    return ctr


# Middle finger
def get_finger_3(glove, detailed_cont):
    base_pts = []
    base_pts.append(glove.contour[(glove.pos + 6) % 11])
    base_pts.append(glove.contour[(glove.pos + 7) % 11])
    base_pts.append(glove.contour[(glove.pos + 8) % 11])

    in_range = False
    all_pts = []
    count = 0
    for i in range(len(detailed_cont)):
        if common.is_cont_in(detailed_cont[i], base_pts):
            in_range = True
            count += 1
        elif count == 3:
            break

        if in_range:
            all_pts.append(detailed_cont[i])

    ctr = np.array(all_pts).reshape((-1, 1, 2)).astype(np.int32)
    return ctr


# Ring finger
def get_finger_4(glove, detailed_cont):
    base_pts = []
    base_pts.append(glove.contour[(glove.pos + 4) % 11])
    base_pts.append(glove.contour[(glove.pos + 5) % 11])
    base_pts.append(glove.contour[(glove.pos + 6) % 11])

    in_range = False
    all_pts = []
    count = 0
    for i in range(len(detailed_cont)):
        if common.is_cont_in(detailed_cont[i], base_pts):
            in_range = True
            count += 1
        elif count == 3:
            break

        if in_range:
            all_pts.append(detailed_cont[i])

    ctr = np.array(all_pts).reshape((-1, 1, 2)).astype(np.int32)
    return ctr


# Pinkie
def get_finger_5(glove, detailed_cont, img):
    base_pts = []

    mid_2_3_x = (glove.contour[(glove.pos + 2) % 11][0][0] + glove.contour[(glove.pos + 3) % 11][0][0]) / 2
    mid_2_3_y = (glove.contour[(glove.pos + 2) % 11][0][1] + glove.contour[(glove.pos + 3) % 11][0][1]) / 2
    min_dist = common.get_distance(mid_2_3_x, mid_2_3_y, detailed_cont[0][0][0], detailed_cont[0][0][1])
    selected = detailed_cont[0]
    for k in detailed_cont:
        dist = common.get_distance(mid_2_3_x, mid_2_3_y, k[0][0], k[0][1])
        if dist < min_dist:
            min_dist = dist
            selected = k
    base_pts.append(selected)
    cv2.circle(img, (mid_2_3_x, mid_2_3_y), 5, (0, 0, 255), -1)
    cv2.circle(img, (selected[0][0], selected[0][1]), 5, (0, 0, 255), -1)

    cv2.circle(img, (glove.contour[(glove.pos + 3) % 11][0][0], glove.contour[(glove.pos + 3) % 11][0][1]), 5,
               (0, 255, 255), -1)
    cv2.circle(img, (glove.contour[(glove.pos + 4) % 11][0][0], glove.contour[(glove.pos + 4) % 11][0][1]), 5,
               (0, 255, 255), -1)

    base_pts.append(glove.contour[(glove.pos + 3) % 11])
    base_pts.append(glove.contour[(glove.pos + 4) % 11])

    all_pts = []
    left_point_met = False
    mid_point_met = False
    right_point_met = False
    first_met = None
    total_cont_count = len(detailed_cont)
    i = 0
    while i <= 2 * total_cont_count:
        current = detailed_cont[i % total_cont_count]
        if current[0][0] == selected[0][0] and current[0][1] == selected[0][1]:
            left_point_met = True
            if first_met is None:
                first_met = 'left'
        elif current[0][0] == glove.contour[(glove.pos + 3) % 11][0][0] and current[0][1] == \
                glove.contour[(glove.pos + 3) % 11][0][1]:
            mid_point_met = True
            if first_met is None:
                first_met = 'mid'
        elif current[0][0] == glove.contour[(glove.pos + 4) % 11][0][0] and current[0][1] == \
                glove.contour[(glove.pos + 4) % 11][0][1]:
            right_point_met = True
            if first_met is None:
                first_met = 'right'

        if left_point_met or right_point_met:
            all_pts.append(current)

        if left_point_met and not mid_point_met and right_point_met:
            all_pts = []
            if first_met == 'left':
                left_point_met = False
            elif first_met == 'right':
                right_point_met = False
        elif first_met == 'mid':
            all_pts = []
            first_met = None
            mid_point_met = False
        elif left_point_met and mid_point_met and right_point_met:
            break

        i += 1

    for p in all_pts:
        cv2.circle(img, (p[0][0], p[0][1]), 2, (255, 0, 255), -1)

    ctr = np.array(all_pts).reshape((-1, 1, 2)).astype(np.int32)
    return ctr


def get_palm(glove, detailed_cont, img):
    b1 = glove.contour[(glove.pos + 1) % 11]
    b2 = glove.contour[(glove.pos + 2) % 11]

    total_cont_count = len(detailed_cont)
    w1 = None
    w2 = None
    w1_min_dist = None
    w2_min_dist = None
    for i in range(total_cont_count):
        if not common.cont_equals(detailed_cont[i], b1) and not common.cont_equals(detailed_cont[i], b2):
            if w1 is None:
                w1 = glove.contour[(glove.pos + 0) % total_cont_count]
                w2 = glove.contour[(glove.pos + 0) % total_cont_count]
                w1_min_dist = common.get_distance(b1[0][0], b1[0][1], w1[0][0], w1[0][1])
                w2_min_dist = common.get_distance(b2[0][0], b2[0][1], w2[0][0], w2[0][1])

            w1_dist = common.get_distance(b1[0][0], b1[0][1], detailed_cont[i][0][0], detailed_cont[i][0][1])
            w2_dist = common.get_distance(b2[0][0], b2[0][1], detailed_cont[i][0][0], detailed_cont[i][0][1])

            if w1_dist < w1_min_dist:
                w1_min_dist = w1_dist
                w1 = detailed_cont[i]
            if w2_dist < w2_min_dist:
                w2_min_dist = w2_dist
                w2 = detailed_cont[i]

    mid_0_1_x = (glove.contour[(glove.pos + 0) % 11][0][0] + glove.contour[(glove.pos + 1) % 11][0][0]) / 2
    mid_0_1_y = (glove.contour[(glove.pos + 0) % 11][0][1] + glove.contour[(glove.pos + 1) % 11][0][1]) / 2
    mid_2_3_x = (glove.contour[(glove.pos + 2) % 11][0][0] + glove.contour[(glove.pos + 3) % 11][0][0]) / 2
    mid_2_3_y = (glove.contour[(glove.pos + 2) % 11][0][1] + glove.contour[(glove.pos + 3) % 11][0][1]) / 2

    cv2.circle(img, (mid_0_1_x, mid_0_1_y), 5, (0, 0, 255), -1)
    cv2.circle(img, (mid_2_3_x, mid_2_3_y), 5, (0, 0, 255), -1)

    all_pts = []
    all_pts.append(w1)
    all_pts.append(w2)
    all_pts.append(glove.contour[(glove.pos + 4) % 11])
    all_pts.append(glove.contour[(glove.pos + 6) % 11])
    all_pts.append(glove.contour[(glove.pos + 8) % 11])
    all_pts.append(glove.contour[(glove.pos + 10) % 11])
    all_pts.append([[mid_0_1_x, mid_0_1_y]])
    all_pts.append([[mid_2_3_x, mid_2_3_y]])

    cv2.circle(img, (w1[0][0], w1[0][1]), 5, (0, 0, 255), -1)
    cv2.circle(img, (w2[0][0], w2[0][1]), 5, (0, 0, 255), -1)

    ctr = np.array(all_pts).reshape((-1, 1, 2)).astype(np.int32)
    return ctr


def get_wrist(glove, detailed_cont, img):
    b1 = glove.contour[(glove.pos + 1) % 11]
    b2 = glove.contour[(glove.pos + 2) % 11]

    total_cont_count = len(detailed_cont)
    w1 = None
    w2 = None
    w1_min_dist = None
    w2_min_dist = None
    for i in range(total_cont_count):
        if not common.cont_equals(detailed_cont[i], b1) and not common.cont_equals(detailed_cont[i], b2):
            if w1 is None:
                w1 = glove.contour[(glove.pos + 0) % total_cont_count]
                w2 = glove.contour[(glove.pos + 0) % total_cont_count]
                w1_min_dist = common.get_distance(b1[0][0], b1[0][1], w1[0][0], w1[0][1])
                w2_min_dist = common.get_distance(b2[0][0], b2[0][1], w2[0][0], w2[0][1])

            w1_dist = common.get_distance(b1[0][0], b1[0][1], detailed_cont[i][0][0], detailed_cont[i][0][1])
            w2_dist = common.get_distance(b2[0][0], b2[0][1], detailed_cont[i][0][0], detailed_cont[i][0][1])

            if w1_dist < w1_min_dist:
                w1_min_dist = w1_dist
                w1 = detailed_cont[i]
            if w2_dist < w2_min_dist:
                w2_min_dist = w2_dist
                w2 = detailed_cont[i]

    all_pts = []
    all_pts.append(w1)
    all_pts.append(b1)
    all_pts.append(b2)
    all_pts.append(w2)

    cv2.circle(img, (w1[0][0], w1[0][1]), 5, (0, 0, 255), -1)
    cv2.circle(img, (w2[0][0], w2[0][1]), 5, (0, 0, 255), -1)

    ctr = np.array(all_pts).reshape((-1, 1, 2)).astype(np.int32)
    return ctr
