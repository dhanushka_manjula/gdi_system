import cv2
import time
import os
import global_values
import traceback
import _defect
import common
import numpy as np
# from androidcamfeed import AndroidCamFeed
import raspberry_pi
from time import sleep
import _image_function
from session import Session
import cache


def pre_process_image(img):
    cache.current_session.gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    cache.current_session.blurred_img_33 = cv2.GaussianBlur(cache.current_session.gray_img, (3, 3), 0)
    returned, cache.current_session.threshold_img_norma_l60 = cv2.threshold(cache.current_session.blurred_img_33, 160,
                                                                            255, cv2.THRESH_BINARY)

    kernel = np.ones((5, 5), np.uint8)
    cache.current_session.eroded_img_55 = cv2.erode(cache.current_session.threshold_img_norma_l60, kernel, iterations=1)
    cache.current_session.dilated_img_55 = cv2.dilate(cache.current_session.threshold_img_norma_l60, kernel,
                                                      iterations=1)

    _, cache.current_session.contours_dilated_list, cache.current_session.hierarchy_dilated_list = cv2.findContours(
        cache.current_session.dilated_img_55.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)


def process_image(img):
    try:
        _image_function.slice_image(img.copy())

        prefect_shape = _defect.perfect_shape(img.copy())
        no_dropped_stitch = _defect.dropped_stitch_new(img.copy())
        no_color_yarn_damage = _defect.color_yarn_damage(img.copy())
        no_needle_run = _defect.needle_run_new(img.copy())
        no_finger_tip_blow_out = _defect.finger_tip_blow_out(img.copy())

        if prefect_shape and no_dropped_stitch and no_color_yarn_damage and no_needle_run and no_finger_tip_blow_out:
            raspberry_pi.indicate_good()
            common.write_image(cache.current_session.img_copy, './output/perfect/' + str(time.time()) + '.jpg',
                               'perfect')
        else:
            raspberry_pi.indicate_bad()
            common.write_image(cache.current_session.img_copy, './output/has_defects/' + str(time.time()) + '.jpg',
                               'has_defects')
    except Exception as e:
        print e
        raspberry_pi.indicate_bad()


# Obsolete
def detect_glove_by_contours(current_frame):
    img = current_frame.copy()

    contours = cache.current_session.contours_dilated_list

    for c in contours:
        cv2.drawContours(img, [c], 0, (255, 0, 0), 2)

    selected_contours = []

    height, width, channels = current_frame.shape
    for c in contours:
        M = cv2.moments(c)
        cx = int(M['m10'] / M['m00'])
        cy = int(M['m01'] / M['m00'])

        approx = cv2.approxPolyDP(c, 0.02 * cv2.arcLength(c, True), True)
        hull = cv2.convexHull(approx, returnPoints=True)

        if len(hull) > 5 and abs(cx - width / 2.0) <= global_values.capture_thresh and abs(
                        cy - height / 2.0) <= global_values.capture_thresh:
            selected_contours.append(c)
            cv2.drawContours(img, [c], 0, (0, 0, 255), 2)

            process_image(current_frame)
            sleep(global_values.capture_delay)
            break

    cv2.imshow('Glove detect', img)


def detect_glove_by_thresh(current_frame):
    img = current_frame.copy()

    threshold_img = cache.current_session.threshold_img_norma_l60

    cv2.imshow('Threshold feed', threshold_img)

    height, width, channels = img.shape

    left_white = True
    for i in range(height):
        if threshold_img[i, 0] == 0:
            left_white = False

    right_white = True
    for i in range(height):
        if threshold_img[i, width - 1] == 0:
            right_white = False

    top_white = True
    for i in range(width):
        if threshold_img[0, i] == 0:
            top_white = False

    bottom_white = True
    for i in range(width):
        if threshold_img[height - 1, i] == 0:
            bottom_white = False

    center = threshold_img[height / 2, width / 2]

    if left_white and right_white and top_white and bottom_white and center == 0 and (not global_values.temp_on_glove):
        global_values.temp_on_glove = True
        common.write_image(img, './output/captured/frame.jpg', time.time())
        return True
    elif left_white and right_white and top_white and bottom_white and center == 0 and global_values.temp_on_glove:
        global_values.temp_on_glove = False
        return False
    else:
        return False


def image_input():
    image_count = 0
    total_time = 0

    folder_list = ['samples']
    # folder_list.append('tie_down')
    # folder_list.append('color_yarn_damage_glove_1')
    # folder_list.append('color_yarn_damage_glove_2')
    # folder_list.append('finger_tip_blow_out')
    # folder_list.append('dropped_stitch')
    # folder_list.append('needle_run')

    for folder in folder_list:
        for f in os.listdir("./input/" + folder):
            if f.endswith(".jpg") or f.endswith(".jpeg"):
                start = time.time()

                new_session = Session()
                cache.current_session = new_session

                img = cv2.imread('./input/' + folder + '/' + str(f))
                cache.current_session.output_path = './output/' + folder + '/' + str(f)
                cache.current_session.img_copy = img.copy()

                try:
                    pre_process_image(img.copy())
                    process_image(img.copy())

                    end = time.time()
                    diff = end - start

                    image_count += 1
                    total_time += diff

                    cv2.putText(img, str(end - start) + ' seconds', (25, 25), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8,
                                (255, 0, 0))
                except:
                    traceback.print_exc()
                    print 'Error in ' + './input/' + folder + '/' + str(f)

                    cv2.putText(img, 'Error occurred', (25, 25), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8,
                                (0, 0, 255))
                finally:
                    common.write_image(img, cache.current_session.output_path, '')
                    pass

    print 'Average processing time per frame =  ' + str(total_time / float(image_count)) + ' s'


def video_input():
    # cam = AndroidCamFeed('192.168.1.13:8080')
    cam1 = cv2.VideoCapture("video5.avi")
    # cam2 = cv2.VideoCapture("video14.avi")
    ret, current_frame1 = cam1.read()
    # ret, current_frame2 = cam2.read()

    while not ret:
        if cam1.isOpened():
            ret, current_frame1 = cam1.read()
    # while not ret:
    #     if cam2.isOpened():
    #         ret, current_frame2 = cam2.read()

    # start_white = 1
    # start_black = 0
    detected = False

    while True:
        ret, current_frame1 = cam1.read()
        # ret, current_frame2 = cam2.read()

        # row = 0
        # height = 0
        # column = 0
        # width = 0
        #
        # current_frame1 = current_frame1_[row: row + height, column: column + width]

        cv2.imshow('Original feed1', current_frame1)
        # cv2.imshow('Original feed2', current_frame2)

        cache.current_session = Session()
        cache.current_session.img_copy = current_frame1.copy()

        pre_process_image(current_frame1.copy())

        if detected and right_colomn_white(current_frame1):
            continue
        else:
            detected = False

        # if right_colomn_white(current_frame1):
        #     if start_black == 0:
        #         pass
        #     else:
        #         if miidle_black(current_frame1):
        #             detected = True
        # else:
        #     start_black = 1


        detected = detect_glove_by_thresh(current_frame1)
        if detected:
            process_image(current_frame1.copy())
            # detected = False
            # cv2.waitKey(0)
        # detected = detect_glove_by_thresh(current_frame2)
        # if detected:
        #     process_image(current_frame2.copy())

        if cv2.waitKey(1) % 256 == 27:
            break

        time.sleep(0.05)

def top_white(img):
    threshold_img = cache.current_session.threshold_img_norma_l60

    cv2.imshow('Threshold feed', threshold_img)

    height, width, channels = img.shape

    top_white_ = True

    for i in range(width):
        if threshold_img[0, i] == 0:
            top_white_ = False

    return top_white_

def right_colomn_white(img):
    right_white = True
    threshold_img = cache.current_session.threshold_img_norma_l60

    height, width, channels = img.shape

    for i in range(height):
        if threshold_img[i, width - 1] == 0:
            right_white = False
            break
    return right_white

def miidle_black(img):
    middle_black = False
    threshold_img = cache.current_session.threshold_img_norma_l60

    height, width, channels = img.shape

    for i in range(height):
        if threshold_img[i, width/2] == 0:
            middle_black = True
            break
    return middle_black


if __name__ == "__main__":
    directories = ['', 'captured', 'good', 'bad', 'has_defects', 'perfect']
    for directory in directories:
        if not os.path.exists('./output/' + directory):
            os.makedirs('./output/' + directory)

    video_input()

